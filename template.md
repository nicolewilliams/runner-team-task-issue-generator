
# Assignments

| What   | Who   |
| ------ | ----- |
| 🐛 🤠 Core Bug Wrangler              | {{ .CoreBugWrangler }}   |
| 🐛 🤠 Fleet Bug Wrangler             | {{ .FleetBugWrangler }}  |
| 🛟 🚒 Support & Security Responder   | {{ .SupportResponder }}  |
| 💬 📌 Community Contribution Triager | {{ .CommunityTriager }}  |
| 💤 💤 Unassigned this week           | {{ .Unassigned }}        |

# Duties

## 🐛 Bug Wranglers 🤠

- [ ] Triage all bugs so that they have the appropriate `~severity::*` and `~priority::*` labels.
- [ ] Tag individual team members or groups (e.g. `@gitlab-com/runner-group/core` or `@gitlab-com/runner-group/fleet`) in issues where you have questions or need additional input.
- [ ] Add a section to the weekly team call outlining issues that are high impact/severity or could benefit from a sync discussion by the team.

## 🛟 Support & Security Responder 🚒

- [ ] Check for newly reported [vulnerabilities](https://app.periscopedata.com/app/gitlab/913607/Past-Due-Security-Issues-Development) attributed to our team. If there are any, then tag the EM, PM, any other relevant parties and make sure they are included in the plan for the next milestone (regardless of severity, unless they are severity 1 in which case drop everything and get them moving). Should be done only once during the week.
- [ ] Check all other newly reported [vulnerabilities](https://app.periscopedata.com/app/gitlab/913607/Past-Due-Security-Issues-Development) within ~"devops::verify" and assess if they are mis-attributed and should be owned by ~"group::runner". If there are, re-label them or start a discussion about re-attribution. If you're confident they should be handled by ~"group::runner" then follow the same process as the previous task. Should be done only once during the week.
- [ ] Monday - Check for new [Support Help Requests](https://gitlab.com/gitlab-com/ops-sub-department/section-ops-request-for-help/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Help%20group%3A%3Arunner&first_page_size=20) and work with the support engineer to resolve their problem.
- [ ] Tuesday - Check for new [Support Help Requests](https://gitlab.com/gitlab-com/ops-sub-department/section-ops-request-for-help/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Help%20group%3A%3Arunner&first_page_size=20) and work with the support engineer to resolve their problem.
- [ ] Wednesday - Check for new [Support Help Requests](https://gitlab.com/gitlab-com/ops-sub-department/section-ops-request-for-help/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Help%20group%3A%3Arunner&first_page_size=20) and work with the support engineer to resolve their problem.
- [ ] Thursday - Check for new [Support Help Requests](https://gitlab.com/gitlab-com/ops-sub-department/section-ops-request-for-help/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Help%20group%3A%3Arunner&first_page_size=20) and work with the support engineer to resolve their problem.
- [ ] Friday - Check for new [Support Help Requests](https://gitlab.com/gitlab-com/ops-sub-department/section-ops-request-for-help/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Help%20group%3A%3Arunner&first_page_size=20) and work with the support engineer to resolve their problem.

## 💬 Community Contribution Triager 📌

- [ ] Check for new Community Contributions and find an appropriate reviewer. Do a first sanity check that they are ready for review and if not work with the MR Coaches to help the contributor.

# Duty Rotations

This issue is created each week by @ajwalker. PTO is considered and a duty is not assigned if more than 3 days off that week.

Contributions can be made at https://gitlab.com/ajwalker/runner-team-task-issue-generator.

## Fleet Bug Wrangler

Alternates weekly between @pedropombeiro and @tschmitke

## All Other Duties

| From | To |
|------|----|
| Core Bug Wrangler              | Support & Security Responder   |
| Support & Security Responder   | Community Contribution Triager | 
| Community Contribution Triager | Unassigned                     |
| Unassigned                      | Core Bug Wrangler              |

/assign {{ .CoreBugWrangler }} {{ .FleetBugWrangler }} {{ .SupportResponder }} {{ .CommunityTriager }}

/labels ~"devops::verify" ~"group::runner" ~"section:ops"

/due {{ .Due }}

<!-- metadata:
{{ . | json }}
-->
